﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PauseGame : MonoBehaviour
{
    //Pause game (freeze time scale)
    public static bool gameIsPaused = false; //is game paused?
    public GameObject PauseMenu; //Pause menu UI

    void Update()
    {
        if(Input.GetKeyDown(KeyCode.Escape)) { //ESC to pause game
            if (gameIsPaused) {
                Resume();
            } else {
                Pause();
            }
        }
    }

    //Resume playing game (hide pause menu and resume timescale)
    public void Resume() {
        Cursor.visible = false;
        gameIsPaused = false;
        AudioListener.pause = false; //Reactivate audiolistener
        PauseMenu.SetActive(false);
        Time.timeScale = 1f;
        //Camera.GetComponent<AudioListener>().enabled = false;
    }

    //Pause game (show pause menu, freeze time)
    void Pause() {
        Cursor.visible = true;
        gameIsPaused = true;
        AudioListener.pause = true; //Disable audio listener
        PauseMenu.SetActive(true);
        Time.timeScale = 0f;
    }
}
