﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrackSelect : MonoBehaviour
{
    public GameObject trackLineup; //Parent object of all spawn point objects
    public static int TrackSelection = 1; //1 = V&A, 2 = TS, 3 = QMB
    private bool hasClicked = false; //If currently moving selection along screen

    void Start() {
        TrackSelection = 1; //Currently on first spawn point
    }

    //Move parent object left
    public void MoveLeft()
    {
        if (!hasClicked && TrackSelection != 1) //If not currently moving and not already at the very left
        {
            hasClicked = true;
            Vector3 newPos = new Vector3(20, 0, 0);
            Vector3 targetLocation = trackLineup.transform.position + newPos;
            float timeDelta = 0.4f;

            StartCoroutine(MoveToPosition(targetLocation, timeDelta));
            TrackSelection -= 1;
            
        }
        Debug.Log("LEFT" + TrackSelection);
    }

    //Move parent object right
    public void MoveRight()
    {
        if (!hasClicked && TrackSelection != 3) //If not currently moving and not already at the very right
        {
            hasClicked = true;
            Vector3 newPos = new Vector3(-20, 0, 0);
            Vector3 targetLocation = trackLineup.transform.position + newPos;
            float timeDelta = 0.4f;

            StartCoroutine(MoveToPosition(targetLocation, timeDelta));
            TrackSelection += 1;
        }
        Debug.Log("RIGHT" + TrackSelection);
    }

    //Lerp track selection along (moves smoother)
    IEnumerator MoveToPosition(Vector3 newPosition, float time)
    {
        float elapsedTime = 0;
        Vector3 startingPos = trackLineup.transform.position;
        while (elapsedTime < time)
        {
            trackLineup.transform.position = Vector3.Lerp(startingPos, newPosition, (elapsedTime / time));
            elapsedTime += Time.deltaTime;
            yield return new WaitForEndOfFrame();
        }
        trackLineup.transform.position = newPosition;
        hasClicked = false;
    }
}
