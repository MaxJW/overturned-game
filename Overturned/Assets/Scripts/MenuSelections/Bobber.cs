﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bobber : MonoBehaviour
{
    //Slowly move spawn point viewers up and down
    public float amplitude = 0.5f; //How far
    public float frequency = 1f; //How often

    private Vector3 posOffset; //Offset to set position by
    private Vector3 tempPos; //Track temporary position
 
    void Start () {
        Cursor.visible = true; //Enable cursor if not already
        posOffset = transform.position; //Set position of spawn object
    }
     
    void Update () {
        posOffset = transform.position;
        tempPos = posOffset;
        tempPos.y += Mathf.Sin (Time.fixedTime * Mathf.PI * frequency) * amplitude; //Calculate y value to adjust by
 
        transform.position = Vector3.Lerp(transform.position, tempPos, Time.deltaTime); //Adjust position
    }
}
