﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HideTrackButton : MonoBehaviour
{
    //Hide buttons if already at far left or far right respectively
    public GameObject leftButton;
    public GameObject rightButton;

    void Start()
    {
        leftButton.SetActive(false);
        rightButton.SetActive(true);
    }

    void Update()
    {
        if (TrackSelect.TrackSelection == 1) {
            leftButton.SetActive(false);
        } else {
            leftButton.SetActive(true);
        }

        if (TrackSelect.TrackSelection == 3) {
            rightButton.SetActive(false);
        } else {
            rightButton.SetActive(true);
        }
    }
}