﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CarSpin : MonoBehaviour
{
    //Was used to slowly rotate cars, currently unused
    void Update()
    {
        transform.Rotate(0, 1, 0, Space.World);
    }
}
