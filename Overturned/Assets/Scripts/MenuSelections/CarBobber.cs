﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CarBobber : MonoBehaviour
{
    //Used to "vibrate" cars
    public float amplitude = 0.5f; //Height
    public float audi_amplitude = 0.5f; //Audi height (looked odd to have the same amount used)
    public float porsche_amplitude = 0.5f; //Porsche height (see above)
    public float frequency = 1f; //How often

    private Vector3 posOffset;
    private Vector3 tempPos;

    private GameObject[] CarBodies; //Array of car bodies

    void Start()
    {
        Cursor.visible = true;
        posOffset = transform.position;

        CarBodies = GameObject.FindGameObjectsWithTag("CarBody"); //Select everything from the model except tyres
    }

    void Update()
    {
        foreach (GameObject carbody in CarBodies) //For each car body, move quickly
        {
            posOffset = carbody.transform.position;
            tempPos = posOffset;
            //Check which car current carbody is a part of
            if (carbody.transform.parent.tag == "SkyCar") {
                tempPos.y += Mathf.Sin(Time.fixedTime * Mathf.PI * frequency) * amplitude;
            } else if (carbody.transform.parent.tag == "AudiCar") {
                tempPos.y += Mathf.Sin(Time.fixedTime * Mathf.PI * frequency) * audi_amplitude;
            } else if (carbody.transform.parent.tag == "PorscheCar") {
                tempPos.y += Mathf.Sin(Time.fixedTime * Mathf.PI * frequency) * porsche_amplitude;
            } else {
                tempPos.y += Mathf.Sin(Time.fixedTime * Mathf.PI * frequency) * amplitude;
            }

            carbody.transform.position = tempPos;
        }
    }
}
