﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HideButton : MonoBehaviour
{
    //Hide left or right button if at end of list of cars
    public GameObject leftButton;
    public GameObject rightButton;
    public GameObject playButton;

    public GlobalCompletion completion; //Current game progression
    public GameObject lockedVehicle; //"Locked" symbol to display in front of locked cars

    void Start()
    {
        leftButton.SetActive(false);
        rightButton.SetActive(true);
    }

    void Update()
    {
        lockedVehicle.SetActive(false);
        playButton.SetActive(true);
        leftButton.SetActive(true);
        rightButton.SetActive(true);

        if (GlobalCar.CarType == 1) { //If at the first vehicle, hide left button (as no more cars to left)
            leftButton.SetActive(false);
        }

        if (GlobalCar.CarType == 3) { //If at the first unlockable car
            if (completion.currentCompletion < 1) { //Check if first race has been completed
                lockedVehicle.SetActive(true);
                playButton.SetActive(false);
            }
        }

        if (GlobalCar.CarType == 4) { //If at last unlockable car
            rightButton.SetActive(false); //Hide right button
            if (completion.currentCompletion < 2) { //Check if second race has also been completed
                lockedVehicle.SetActive(true);
                playButton.SetActive(false);
            }
        }
    }
}
