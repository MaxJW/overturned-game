﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Playables;

public class MenuClick : MonoBehaviour
{
    //Main menu button operations

    public GameObject menu; //Main Menu hologram
    public GameObject menuLight; //Hologram light
    public GameObject sparkObject; //Sparks shown when hologram flickers
    public GameObject flicker; //Flicker event
    public AudioSource sparkEffect; //Spark Audio source
    public GameObject MainMenuUI; //Specific main menu options (Play game, options etc.)
    public GameObject OptionsUI; //Specific options menu (Resolution, quality etc.)
    public PlayableDirector director; //Director to start animation of pedestal rising

    //Initialise Game
    public void PlayGame()
    {
        //Disable Menu
        sparkObject.GetComponent<ParticleSystem>().Play();
        sparkEffect.Play();
        menu.SetActive(false);
        menuLight.SetActive(false);
        //Disable Flicker
        flicker.SetActive(false);

        director.Play(); //Play animation

        //SceneManager.LoadScene("CarSelection"); //Backup incase game does not load
    }

    //Options menu loader
    public void Options()
    {
        flicker.GetComponent<Flicker>().stopped = true; //Stop flicker temporarily (otherwise annoying to try and select options)
        MainMenuUI.SetActive(false); //Disable main menu
        OptionsUI.SetActive(true); //Enable options menu
    }

    //Play credits
    public void Credits()
    {
        SceneManager.LoadScene("Credits");
    }

    //Quit game
    public void Quit()
    {
        Application.Quit();
    }

    //Return back to main menu
    public void BackToMenu() {
        flicker.GetComponent<Flicker>().stopped = false;
        OptionsUI.SetActive(false);
        MainMenuUI.SetActive(true);
    }
}
