﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class AnimationEvents : MonoBehaviour
{
    //Load Car Selection once PlayGame has been pressed and animation is completed
    public void PlayGame() {
        SceneManager.LoadScene("CarSelection");
    }
}
