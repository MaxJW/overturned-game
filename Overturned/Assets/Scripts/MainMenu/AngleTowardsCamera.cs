﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AngleTowardsCamera : MonoBehaviour
{
    public Transform target; //Target to angle towards
    public bool levelIcon; //If a level icon (loaders within openworldscene)

    void Update() {
        transform.LookAt(target); //Look at target
        if (levelIcon) { //If level icon, rotate to face player correctly
            Vector3 rotate = transform.rotation.eulerAngles;
            rotate.x += 100;
            transform.rotation = Quaternion.Euler(rotate);
        }
    }
}
