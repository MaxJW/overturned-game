﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;

public class Flicker : MonoBehaviour
{
    //Used to flicker the hologram display
    public float startDelay = 4f; //Delay before first flicker
    public bool stopped = false; //Flicker disabled?
    private bool blinking = false; //Currently flickering?

    public GameObject menu; //Hologram menu
    public GameObject menuLight; //Holgoram light
    public GameObject sparkObject; //Spark effect

    public AudioSource sparkEffect; //Spark audio source
    public AudioSource backgroundMusic; //Background music

    void Start()
    {
        Time.timeScale = 1f; //Reset timescale if loading in from pause menu
        AudioListener.pause = false; //Enable audio listener
        Cursor.visible = true; //Show cursor
        Debug.Log("start");
        backgroundMusic.Play(); //In case background music does not play on awake (occurs infrequently)

        Invoke("Blink", startDelay); //Begin flickering effect
    }

    //Flicker effect
    void Blink()
    {
        if (!stopped) //If not currently stopped (e.g. in options menu)
        {
            float viewTime = Random.Range(3f, 12f); //How long to display menu for
            float offTime = Random.Range(0.1f, 0.2f); //How long to hide hologram for
            Debug.Log(viewTime);
            Debug.Log(offTime);

            if (!blinking) //If not currently hidden
            {
                Debug.Log("off");

                //Flicker
                sparkObject.GetComponent<ParticleSystem>().Play();
                sparkEffect.Play();
                menu.SetActive(false);
                menuLight.SetActive(false);

                blinking = true;
                Invoke("Blink", offTime); //Invoke blink again
            }
            else
            {
                Debug.Log("on");

                //Reenable menu
                menu.SetActive(true);
                menuLight.SetActive(true);

                blinking = false;

                Invoke("Blink", viewTime); //Invoke blink again
            }
        } else {
            Invoke("Blink", 5f); //Check if it has been stopped every 5 seconds
        }
    }

    void OnDisable()
    {
        CancelInvoke(); //Cancel invoke if stopped
    }
}
