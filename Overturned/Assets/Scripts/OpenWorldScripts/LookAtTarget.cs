﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LookAtTarget : MonoBehaviour
{
    public GameObject target; //Target to look at

    void Update()
    {
        transform.LookAt(target.transform); //Look at target
    }
}
