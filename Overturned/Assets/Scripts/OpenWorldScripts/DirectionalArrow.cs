﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DirectionalArrow : MonoBehaviour
{
    public GameObject pointer; //Pointer attached to player to point at object selected

    void Update()
    {
        this.transform.rotation = pointer.transform.localRotation; //Set rotation equal to that of the pointer
        //transform.rotation = Quaternion.Euler(lockX, pointer.transform.localRotation.y, pointer.transform.localRotation.z); //Does not work
    }
}