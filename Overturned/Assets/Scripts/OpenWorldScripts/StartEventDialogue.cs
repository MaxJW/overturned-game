﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class StartEventDialogue : MonoBehaviour
{
    public GameObject eventPanel;
    public TextMeshProUGUI eventTitle;
    public TextMeshProUGUI eventDescription;
    public TextMeshProUGUI eventButtonText;
    public Button eventButton;

    public ButtonOptions ButtonOptions;
    public RaceStarter RaceButtonOptions;

    private EventInfo eventInfo;

    private bool triggered = false;
    private int eventType;

    void Start()
    {
        eventPanel.SetActive(false);
        eventInfo = GetComponent<EventInfo>(); //Get event details for current object
    }

    void OnTriggerEnter(Collider carObject)
    {
        //When entering loader, display correct information and adjust button to start correct event
        //Debug.Log(carObject.gameObject.tag);
        if (carObject.gameObject.tag == "Player" && triggered != true)
        {
            eventTitle.text = eventInfo.EventName;
            eventDescription.text = eventInfo.Description;
            eventButtonText.text = eventInfo.ButtonText;
            eventButton.onClick.RemoveAllListeners();
            if (eventInfo.EventType == 0)
            {
                if (eventInfo.RaceNo == 1)
                {
                    eventButton.onClick.AddListener(RaceButtonOptions.StartRace01);
                }
                else if (eventInfo.RaceNo == 2)
                {
                    eventButton.onClick.AddListener(RaceButtonOptions.StartRace02);
                }
            }
            else if (eventInfo.EventType == 1)
            {
                eventButton.onClick.AddListener(ButtonOptions.BattleRacePlaceholder);
            }
            else if (eventInfo.EventType == 2)
            {
                eventButton.onClick.AddListener(ButtonOptions.Garage);
            }
            else if (eventInfo.EventType == 3)
            {
                eventButton.onClick.AddListener(ButtonOptions.Weapon);
            }

            triggered = true;
            eventPanel.SetActive(true);
            Fader(true);
        }
    }

    void OnTriggerExit(Collider carObject)
    {
        //Hide panel on trigger exit
        if (carObject.gameObject.tag == "Player")
        {
            DeactivatePanel();
        }
    }

    //Fade panel in (smooths instead of popping in and out)
    void Fader(bool into)
    {
        if (into)
        {
            eventPanel.GetComponent<CanvasRenderer>().SetAlpha(0.1f);
            eventPanel.GetComponent<Image>().CrossFadeAlpha(1f, .25f, false);
        }
        else
        {
            eventPanel.GetComponent<CanvasRenderer>().SetAlpha(1f);
            eventPanel.GetComponent<Image>().CrossFadeAlpha(0.1f, .1f, false);
            eventPanel.SetActive(false);
        }
    }

    //Hide panel
    public void DeactivatePanel()
    {
        triggered = false;
        eventPanel.SetActive(false);
        Fader(false);
    }
}
