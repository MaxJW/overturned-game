﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AlignToGround : MonoBehaviour
{
    //Align game loaders with terrain bumps
    private Terrain TerrainGame; //Terrain
    private Vector3 newPos; //Position to adjust to

    public float hover = 0.1f; //Amount to raise object above ground

    void Start()
    {
        //Set y position of loader to terrain below
        TerrainGame = Terrain.activeTerrain; //Get current terrain
        newPos = new Vector3(transform.position.x, 0, transform.position.z); 

        newPos.y = TerrainGame.SampleHeight(transform.position) + Terrain.activeTerrain.GetPosition().y + hover;
        transform.position = newPos;

        //Send out raycast to get normal of terrain below, rotate loader accordingly
        RaycastHit hit;

        if (Physics.Raycast(transform.position, -Vector3.up, out hit))
        {
            Quaternion grndTilt = Quaternion.FromToRotation(Vector3.up, hit.normal);
            transform.rotation = grndTilt * Quaternion.Euler(0, 1, 0);
        }
        //Debug.Log(newPos.y);
    }
}