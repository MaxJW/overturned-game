﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnterButton : MonoBehaviour
{
    void Update()
    {
        if (Input.GetKeyDown("return")) { //Invoke button when enter key is pressed
            this.gameObject.GetComponent<Button>().onClick.Invoke();
        }
    }
}
