﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityStandardAssets.Vehicles.Car;

public class Speedometer : MonoBehaviour
{
    public GameObject drivableCar;
    public Image MPHBarImage;
    public GameObject gears;
    public GameObject MPH;
    public int TopSpeed;
    
    private CarController m_CarController;

    void Start()
    {
        m_CarController = drivableCar.GetComponent<CarController>(); //Get CarController script
    }

    void Update()
    {
        //gears.GetComponent<TextMeshProUGUI>().text = "1";// + (CarController.m_GearNum+1);
        gears.GetComponent<TextMeshProUGUI>().text = "" + (m_CarController.CurrentGear); //Get current gear of car

        float speedo = m_CarController.CurrentSpeed;
        MPH.GetComponent<TextMeshProUGUI>().text = "" + speedo.ToString("F0"); //Convert speed to string

        float bartotal = speedo/TopSpeed; //Get percentage of total speed possible
        float bartotal_convert = bartotal+0.07f; //Adjust for speedometer bar
        //Debug.Log("CS: " + CurrentSpeed + " MS: " + MaxSpeed + " f:" + f + " F_C: " + f_convert);
        MPHBarImage.fillAmount = bartotal_convert; //Set speedometer bar fill amount appropriately
    }
}
