﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthController : MonoBehaviour
{
    private float startHealth; //Starting health
    private float health = 100; //Current health
    public float offset = 3f; //HUD offset from vehicles
    public GameObject healthUIElement; //HealthHUD component
    public Slider PlayerHealth; //Health slider (Player)
    public GameObject canvas; //The game canvas

    private Slider healthSlider; //Health slider (CPU)
    private GameObject hpBar; //HealthHUD to display above computers

    public float currentHealth { get { return health; }}

    void Start()
    {
        if (gameObject.tag == "Player") //If player, set health to durability
        {
            startHealth = GlobalCarStatistics.durability;
            health = startHealth;
        }
        else
        {
            startHealth = 100; //Otherwise set default health (could later be changed)
            health = startHealth;
        }

        Debug.Log("Health: " + health);
        FloatingHealthBar(); //Initiate health bars above opponents
    }

    void Update()
    {
        if (hpBar != null) //If computer has not been destroyed, display healthbar above computer vehicle
        {
            hpBar.transform.position = Camera.main.WorldToScreenPoint((Vector3.up * offset) + transform.position);
        }
    }

    void OnBecameVisible()
    {
        //Debug.Log("visible");
        if (hpBar != null) //Show health bar if computer vehicle is viewable by player
        {
            hpBar.SetActive(true);
        }
    }

    void OnBecameInvisible()
    {
        //Debug.Log("invisible");
        if (hpBar != null) //Hide health bar if computer vehicle is not viewable by player
        {
            hpBar.SetActive(false);
        }
    }

    public void TakeDamage(float amount)
    {
        health -= amount; //Remove amount from health
        healthSlider.value = health / startHealth; //Adjust slider
        Debug.Log(health);
        if (health <= 0)
        {
            Die(); //If out of health, remove player
        }
    }

    void Die()
    {
        Destroy(hpBar); //Remove healthbar UI
        Destroy(gameObject); //Remove computer from game
    }

    //Instantiate a health bar above each player
    void FloatingHealthBar()
    {
        if (tag != "Player") //If a computer player, create hud instance above vehicle
        {
            hpBar = Instantiate(healthUIElement, transform.position, transform.rotation) as GameObject;
            hpBar.transform.SetParent(canvas.transform, false);
            GameObject tempSlider = hpBar.transform.Find("HealthBar").gameObject;
            healthSlider = tempSlider.GetComponent<Slider>();
        }
        else
        {
            healthSlider = PlayerHealth; //Otherwise, set health slider
        }
    }
}
