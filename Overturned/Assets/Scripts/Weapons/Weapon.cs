﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Weapon : MonoBehaviour
{
    public GameObject bulletObject;
    public GameObject barrel;

    public float aimSpeed = 1f; 
    public float shotSpeed = 500f;
    public float damage = 1f;
    public float range = 100f;
    public float destructionTime = 3f; //Time before destroy bullet object instance
    public float impactForce = 30f;
    public float fireRate = 15f;

    //Limit gun rotations
    public float minimumX = -90F;
    public float maximumX = 90F;

    Quaternion originalRotation;

    public Texture2D crosshairImage; //Crosshair cursor
    private bool hitSomething = false;
    private float nextTimeToFire = 0f; //Limit fireate

    void Start()
    {
        originalRotation = transform.localRotation;
        Cursor.visible = false;
    }

    void Update()
    {
        //Ray debug draw
        Debug.DrawRay(barrel.transform.position, barrel.transform.forward * range, Color.red);
        RaycastHit rhInfo;

        Vector3 shootToward;
        Ray barrelRay = new Ray(barrel.transform.position, barrel.transform.forward); //Cast ray to check what is being hit within range limit

        hitSomething = Physics.Raycast(barrelRay, out rhInfo, range);

        if (hitSomething)
        {
            shootToward = rhInfo.point;
        }
        else
        {
            shootToward = barrelRay.origin + barrelRay.direction * range;
        }

        //screenPos.y = Screen.height - screenPos.y;
        /*if (aimCursor != null)
        {
            aimCursor.transform.position = Vector3.Lerp(aimCursor.transform.position, shootToward, Time.deltaTime * aimSpeed);
            
        }*/

        /* rotationY += Input.GetAxis("Mouse Y");
        rotationX += Input.GetAxis("Mouse X");

        rotFinalY = ClampAngle(rotationY, minimumY, maximumY);
        rotFinalX = ClampAngle(rotationX, minimumX, maximumX);

        Quaternion yQuaternion = Quaternion.AngleAxis(rotFinalY, Vector3.left);
        Quaternion xQuaternion = Quaternion.AngleAxis(rotFinalX, Vector3.up);

        transform.localRotation = originalRotation * xQuaternion * yQuaternion; */

        Ray mouseRay = Camera.main.ScreenPointToRay(Input.mousePosition);
        Vector3 angler;
        if (Physics.Raycast(mouseRay, out rhInfo, range))
        {
            angler = rhInfo.point;
        }
        else
        {
            angler = mouseRay.origin + mouseRay.direction * 50.0f;
        }
        Vector3 direction = angler - transform.position;
        direction.y = ClampAngle(direction.y, minimumX, maximumX);
        Quaternion wantedRotation = Quaternion.LookRotation(direction);
        transform.rotation = Quaternion.Slerp(transform.rotation, wantedRotation, Time.deltaTime * aimSpeed);

        if (Input.GetButton("Fire1") && Time.time >= nextTimeToFire)
        {
            nextTimeToFire = Time.time + 1f / fireRate;
            Shoot();
            GameObject shotGO = (GameObject)Instantiate(bulletObject, barrel.transform.position, barrel.transform.rotation);
            Vector3 deltaPos = shootToward - shotGO.transform.position;
            shotGO.GetComponent<Rigidbody>().velocity = deltaPos.normalized * shotSpeed;
            Destroy(shotGO, destructionTime);
        }
    }

    void Shoot()
    {
        RaycastHit hit;

        if (Physics.Raycast(barrel.transform.position, barrel.transform.forward, out hit, range))
        {
            Debug.Log(hit.transform.name);
            if (hit.transform != transform.parent.parent)
            {
                HealthController target = hit.transform.GetComponent<HealthController>();
                if (target != null)
                {
                    target.TakeDamage(damage);
                }

                if (hit.rigidbody != null)
                {
                    hit.rigidbody.AddForce(-hit.normal * impactForce);
                }

                //GameObject impactGO = Instantiate(impactEffect, hit.point, Quaternion.LookRotation(hit.normal));
                //Destroy(impactGO, 2f);
            }
        }
    }

    public static float ClampAngle(float angle, float min, float max)
    {
        angle = angle % 360;
        if ((angle >= -360F) && (angle <= 360F))
        {
            if (angle < -360F)
            {
                angle += 360F;
            }
            if (angle > 360F)
            {
                angle -= 360F;
            }
        }
        return Mathf.Clamp(angle, min, max);
    }

    //Draw crosshair where mouse is
    void OnGUI()
    {
        float xMin = Screen.width - (Screen.width - Input.mousePosition.x) - ((crosshairImage.width * 3) / 2);
        float yMin = (Screen.height - Input.mousePosition.y) - ((crosshairImage.height * 3) / 2);
        GUI.DrawTexture(new Rect(xMin, yMin, crosshairImage.width * 3, crosshairImage.height * 3), crosshairImage);
    }
}
